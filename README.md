# SBC: Shade, Blend, Convert

This is a small PHP library for shading, blending and converting colors. It is based on the [javascript library](https://github.com/PimpTrizkit/PJs/blob/master/pSBC.js) that was created by [PimpTrizkit](https://github.com/PimpTrizkit) following his [answer and question on StackOverflow](https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors/13542669#13542669).

This library provides the `SBC` class containing 5 static methods:

 - `SBC::parse(string $s_color) : array` parses a color
 - `SBC::ShadeBlendConvert(float $f_ratio, string $s_from, ?string $s_to = null) : ?string` shades, blends or converts the color(s) based on what parameters you pass
 - `SBC::Shade(float $f_ratio, string $s_from) : ?string` shades a color
 - `SBC::Blend(float $f_ratio, string $s_from, string $s_to) : ?string` blends two colors
 - `SBC::Convert(string $s_color) : ?string` converts between color notations

See the docblocks in `SBC.php` for detailed explanations

## Installation

You can find [this library](https://gitlab.com/marcovo/shade-blend-convert) on [packagist](https://packagist.org/packages/marcovo/shade-blend-convert), so you can install it using [composer](https://getcomposer.org/) using this command:
```
composer require marcovo/shade-blend-convert
```
