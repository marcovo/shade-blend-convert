<?php

/**
 * Utility class for Shading, Blending and Converting colors.
 * 
 * Original source & implementations in JS can be found on:
 * https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors/13542669#13542669
 * https://github.com/PimpTrizkit/PJs/wiki/12.-Shade,-Blend-and-Convert-a-Web-Color-(pSBC.js)
 */
class SBC {

	/**
	 * Parse a color specification
	 * 
	 * @param string $s_color  Either #rgb, #rgba, #rrggbb, #rrggbbaa, rgb(r, g, b) or rgba(r, g, b, a),
	 *                         as used in CSS
	 * 
	 * @return ?array Color components as in the rgba() notation: [r, g, b, a] where r, g, b are from
	 *                [0, 255] and a is from [0, 1]. Will return null on error
	 */
	public static function parse($s_color) {
		$i_length = strlen($s_color);
		$a_rgb = [];
		
		if ($i_length > 9) {
			// rgb or rgba specification
			$a_parts = explode(',', $s_color);
			if (count($a_parts) < 3 || count($a_parts) > 4)
				return null;
			
			$a_rgb[0] = (int)(explode('(', $a_parts[0])[1]);
			$a_rgb[1] = (int)($a_parts[1]);
			$a_rgb[2] = (int)($a_parts[2]);
			
			if(isset($a_parts[3])) {
				$a_rgb[3] = (float)$a_parts[3];
			}
		}
		else {
			// hash # specification
			// either #rgb (4), #rgba (5), #rrggbb (7) or #rrggbbaa (9)
			if ($i_length == 8 || $i_length == 6 || $i_length < 4)
				return null;
			
			if ($i_length < 6)
				$s_color = "#" .
					$s_color[1] . $s_color[1] .
					$s_color[2] . $s_color[2] .
					$s_color[3] . $s_color[3] .
					(($i_length == 5) ? $s_color[4] . $s_color[4] : ""); //3 or 4 digit
			
			$a_rgb[0] = intval(substr($s_color, 1, 2), 16);
			$a_rgb[1] = intval(substr($s_color, 3, 2), 16);
			$a_rgb[2] = intval(substr($s_color, 5, 2), 16);
			if($i_length == 9 || $i_length == 5) {
				$a_rgb[3] = intval(substr($s_color, 7, 2), 16)/255;
			}
			
		}
		return $a_rgb;
	}

	/**
	 * Convert a color. The returned color will be in the format used by $s_to if specified, or the format
	 * used by $s_from otherwise. If $s_to equals 'c', the other format will be returned
	 * 
	 * @param float   $f_ratio Number from the range [0, 1], indicating what percentage to take of both
	 *                         colors. 0 indicates full $s_from, while 1 indicates full $s_to
	 * @param string  $s_from  A color as defined by parse()
	 * @param ?string $s_to    A color as defined by parse(). Optional, if not passed defaults to white
	 *                         or black. May also equal 'c' to change color notation of the output when
	 *                         shading. When equal to 'c' or null, $f_ratio may range from -1 to 1,
	 *                         negative values indicating shading to black, non-negative values indicating
	 *                         shading to white.
	 * 
	 * @return ?string The converted color, or null on error
	 */
	public static function ShadeBlendConvert($f_ratio, $s_from, $s_to = null) {
		if (!is_numeric($f_ratio) || $f_ratio < -1 || $f_ratio > 1)
			return null;
			
		if (!is_string($s_from) || $s_from[0] != 'r' && $s_from[0] != '#')
			return null;
			
		if (!is_null($s_to) && !is_string($s_to))
			return null;
		
		$b_rgba = strlen($s_from) > 9;
		
		if(!is_null($s_to)) {
			if($s_to == "c") {
				$b_rgba = !$b_rgba;
			}
			else {
				$b_rgba = (strlen($s_to) > 9);
			}
		}
		
		if(is_null($s_to) || $s_to == "c") {
			$s_to = ($f_ratio < 0) ? "#000000" : "#FFFFFF";
		}
		
		$f_ratio = abs($f_ratio);
		$a_from = static::parse($s_from);
		$a_to = static::parse($s_to);
		if (!$a_from || !$a_to)
			return null;
		
		$a_result = [
			($a_to[0] - $a_from[0]) * $f_ratio + $a_from[0],
			($a_to[1] - $a_from[1]) * $f_ratio + $a_from[1],
			($a_to[2] - $a_from[2]) * $f_ratio + $a_from[2],
		];
		
		if(isset($a_to[3]) && isset($a_from[3])) {
			$a_result[3] = ($a_to[3] - $a_from[3]) * $f_ratio + $a_from[3];
		}
		else if(isset($a_to[3])) {
			$a_result[3] = $a_to[3];
		}
		else if(isset($a_from[3])) {
			$a_result[3] = $a_from[3];
		}
		
		if ($b_rgba)
			return
				(isset($a_result[3]) ? "rgba(" : "rgb(")
				. round($a_result[0]) . ","
				. round($a_result[1]) . ","
				. round($a_result[2]) .
				(!isset($a_result[3]) ? ")" : "," . round($a_result[3] * 10000) / 10000 . ")");
		else {
			
			return '#'.
				str_pad(dechex(round($a_result[0])), 2, '0', STR_PAD_LEFT)
			  . str_pad(dechex(round($a_result[1])), 2, '0', STR_PAD_LEFT)
			  . str_pad(dechex(round($a_result[2])), 2, '0', STR_PAD_LEFT)
			  . (!isset($a_result[3]) ? '' : str_pad(dechex(round($a_result[3] * 255)), 2, '0', STR_PAD_LEFT));
		}
	}

	/**
	 * Shade a color
	 * 
	 * @param float  $f_ratio  Number from the range [-1, 1], indicating how strong the shading should
	 *                         be applied. Negative numbers indicate shading to black, non-negative
	 *                         numbers indicate shading to white
	 * @param string $s_from   The color to be shaded, as defined by parse()
	 * 
	 * @return string The shaded color, or null on error
	 */
	public static function Shade($f_ratio, $s_from) {
		return static::ShadeBlendConvert($f_ratio, $s_from);
	}

	/**
	 * Blend two colors. The returned color will be in the format used by $s_to
	 * 
	 * @param float  $f_ratio  Number from the range [0, 1], indicating what percentage to take of both
	 *                         colors. 0 indicates full $s_from, while 1 indicates full $s_to
	 * @param string $s_from   A color as defined by parse()
	 * @param string $s_to     A color as defined by parse()
	 * 
	 * @return string The blended color, or null on error
	 */
	public static function Blend($f_ratio, $s_from, $s_to) {
		return static::ShadeBlendConvert($f_ratio, $s_from, $s_to);
	}

	/**
	 * Convert a color from one notation to another
	 * 
	 * @param string $s_from   The color to be converted, as defined by parse()
	 * 
	 * @return string The converted color, or null on error
	 */
	public static function Convert($s_from) {
		return static::ShadeBlendConvert(0, $s_from, 'c');
	}
}
