<?php

//require_once(__DIR__ . '/SBC.php');

require_once('./vendor/autoload.php');

// Some tests as can be found on:
// https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors/13542669#13542669

function shade($s_baseColor, $b_lighten) {
	for($i=0;$i<=10;$i++) {
		$f_ratio = ($b_lighten ? 1 : -1)*$i/10;
		yield $f_ratio => SBC::Shade($f_ratio, $s_baseColor);
	}
}

function blend($s_color1, $s_color2) {
	for($i=0;$i<=10;$i++) {
		$f_ratio = $i/10;
		yield $f_ratio => SBC::Blend($f_ratio, $s_color1, $s_color2);
	}
}

function colorTable($a_columns) {
	$s_table = '<table>';
	for($i=0;$i<=10;$i++) {
		$s_table .= '<tr>';
		
		foreach($a_columns as $a_column) {
			$s_textColor = $a_column[0];
			$generator = $a_column[1];
			
			$s_table .= '
			<td style="background-color: '.$generator->current().'; color: '.$s_textColor.';">
				' . $generator->key() . '<br />
				' . $generator->current() . '
			</td>';
			
			$generator->next();
		}
		
		$s_table .= '</tr>';
	}
	$s_table .= '</table>';
	
	return $s_table;
}

?>
<!DOCTYPE html>
<html>
<head>
<style>
body {
	font-family: courier;
	font-size: 11pt;
}

table {
	border-collapse: collapse;
}

td {
	padding: 5px;
}
</style>
</head>
<body>
<table>
	<tr>
		<td>
			Shade positive
			<?php
			echo(colorTable([
				['#000', shade('#3f83a3', true)],
				['#000', shade('#f48000', true)],
			]));
			?>
		</td>

		<td>
			Shade negative
			<?php
			echo(colorTable([
				['#fff', shade('#3f83a3', false)],
				['#fff', shade('#f48000', false)],
			]));
			?>
		</td>

		<td>
			Blend
			<?php
			echo(colorTable([
				['#000', blend('#3f83a3', '#f48000')],
				['#000', blend('#f48000', '#3f83a3')],
			]));
			?>
		</td>
	</tr>
</table>

</body>
</html>
