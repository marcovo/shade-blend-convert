<?php

//require_once(__DIR__ . '/SBC.php');

require_once('./vendor/autoload.php');


// Some tests as can be found on:
// https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors/13542669#13542669

$color1 = "rgb(20,60,200)";
$color2 = "rgba(20,60,200,0.67423)";
$color3 = "#67DAF0";
$color4 = "#5567DAF0";
$color5 = "#F3A";
$color6 = "#F3A9";
$color7 = "rgb(200,60,20)";
$color8 = "rgba(200,60,20,0.98631)";

// Shade (Lighten or Darken)
var_dump(SBC::ShadeBlendConvert ( 0.42, $color1 )); // rgb(20,60,200) + [42% Lighter] => rgb(119,142,223)
var_dump(SBC::ShadeBlendConvert ( -0.4, $color5 )); // #F3A + [40% Darker] => #991f66
var_dump(SBC::ShadeBlendConvert ( 0.42, $color8 )); // rgba(200,60,20,0.98631) + [42% Lighter] => rgba(223,142,119,0.98631)
// Shade with Conversion (use "c" as your "to" color)
var_dump(SBC::ShadeBlendConvert ( 0.42, $color2, "c" )); // rgba(20,60,200,0.67423) + [42% Lighter] + [Convert] => #778edfac
// RGB2Hex & Hex2RGB Conversion Only (set percentage to zero)
var_dump(SBC::ShadeBlendConvert ( 0, $color6, "c" )); // #F3A9 + [Convert] => rgba(255,51,170,0.6)
// Blending
var_dump(SBC::ShadeBlendConvert ( -0.5, $color2, $color8 )); // rgba(20,60,200,0.67423) + rgba(200,60,20,0.98631) + [50% Blend] => rgba(110,60,110,0.8303)
var_dump(SBC::ShadeBlendConvert ( 0.7, $color2, $color7 )); // rgba(20,60,200,0.67423) + rgb(200,60,20) + [70% Blend] => rgba(146,60,74,0.67423)
var_dump(SBC::ShadeBlendConvert ( 0.25, $color3, $color7 )); // #67DAF0 + rgb(200,60,20) + [25% Blend] => rgb(127,179,185)
var_dump(SBC::ShadeBlendConvert ( 0.75, $color7, $color3 )); // rgb(200,60,20) + #67DAF0 + [75% Blend] => #7fb3b9
// Error Checking
var_dump(SBC::ShadeBlendConvert ( 0.42, "#FFBAA" )); // #FFBAA + [42% Lighter] => null  (Invalid Input Color)
var_dump(SBC::ShadeBlendConvert ( 42, $color1, $color5 )); // rgb(20,60,200) + #F3A + [4200% Blend] => null  (Invalid Percentage Range)
var_dump(SBC::ShadeBlendConvert ( 0.42, [] )); // [object Object] + [42% Lighter] => null  (Strings Only for Color)
var_dump(SBC::ShadeBlendConvert ( "42", $color1 )); // rgb(20,60,200) + ["42"] => null  (Numbers Only for Percentage)
var_dump(SBC::ShadeBlendConvert ( 0.42, "salt" )); // salt + [42% Lighter] => null  (A Little Salt is No Good...)
// Error Check Fails (Some Errors are not Caught)
var_dump(SBC::ShadeBlendConvert ( 0.42, "#salt" )); // #salt + [42% Lighter] => #6b6b6b00  (...and a Pound of Salt is Jibberish)
// Ripping
var_dump(SBC::parse ( $color4 )); // #5567DAF0 + [Rip] => [object Object] => {'0':85,'1':103,'2':218,'3':0.9412}
